const HDIRequest = require('./request');
const config = require('./config');

const createUrlPayment = async (req, res, next)=>{
	const pay_id = req.body.ID
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/payment/createUrlPay', req.body, "SEARCH_INSUR_INFO")
		if(response.Data){
			if(response.Data.URL_REDIRECT){
				return res.send({success: true, url_redirect: response.Data.URL_REDIRECT })
			}else{
				console.log("Create Pay Err:  ", pay_id, response)
				return res.send({success: false})
			}
		}else{
			console.log("Create Pay Err:  ", pay_id, response)
			return res.send({success: false})
		}
		
	}catch(e){
		console.log("createUrlPayment Error ",pay_id, e)
		return res.send({success:false})
	}
}

module.exports = {
	createUrlPayment
}