require('dotenv').config()

const envVariables = {
  domain1: process.env.DM1,
  domain2: process.env.DM2,
  app:{
    ENVIRONMENT: "LIVE",
  },
  auth:{
      deviceCode: "",
      deviceEnvironment: "WEB",
      ipPrivate: "123",
      parent_code: process.env.PTN_CODE,
      username: process.env.USR_NAME,
      secret: process.env.SECRET,
      password: process.env.PASSWORD,
      CLIENT_ID: process.env.CLIENT_ID,
      token: null,
      lastLogin: 0,
      expireIn: 0
  }
}
module.exports = envVariables;