const Router = require('express').Router();
const Controller = require('./PaymentController');
Router.post('/api/create-url-payment', Controller.createUrlPayment)

module.exports = Router;