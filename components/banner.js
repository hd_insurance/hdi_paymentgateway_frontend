
export default ()=>(
	<div className="banner">
		<div className="child-bg">
			<div className="container">
				<div className="row">
					<div className="col-md-6" style={{textAlign: 'center'}}>
						<img data-aos="slide-right" className="app-screenshot" src="/img/app-phone-screenshot.png" />
					</div>
					<div className="col-md-6">
						<img className="logo-hdi-white" src="/img/logo-hdi-white.png" />
						<h1>Tận hưởng cuộc sống</h1>
						<h2>Vững tâm tiến bước với <b>Happy Digital Life</b></h2>
						<p>Ứng dụng hàng đầu về bảo hiểm phi nhân thọ tại Việt Nam.</p> <p>Tải App ngay hôm nay để nhận những ưu đãi đặc biệt từ Happy Digital Life</p>
						<div className="app-download">
							<div className="btn-download-group">
								<img src="/img/GooglePlay.png" />
								<img className="mg-20" src="/img/AppleStore.png" />
							</div>
							<div className="app-dive-text">Hoặc</div>
							<div className="img-download-qr">
								<img className="qr-app" src="/img/websiteQRCode_noFrame.png" />
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	)