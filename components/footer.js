import Link from 'next/link';
import { Modal, Button } from 'react-bootstrap';
import React, {
  useState
} from "react";
const Footer = (props) => {
   const [open1, setOpen1] = useState(false);
   const [open2, setOpen2] = useState(false);

   const openMd1 = ()=>{
      if(open2){
         setOpen2(false)
      }
      setOpen1(true)
   }
   const openMd2 = ()=>{
      setOpen2(true)
   }
   return (<div className="footer">
       <div className="row">
            <div className="col-md-6 menu-left-group">
               <p>Copyright © 2021 HDInsurance all rights reserved</p>
               <div className="menu-link-gr">
                  <div><a onClick={openMd1}>Điều khoản bảo mật</a></div>
                  <div><a onClick={openMd2}>Chính sách thanh toán</a></div>
               </div>
            </div>
            <div className="col-md-6">
               <div className="right">
                  <p><i class="fas fa-headphones-alt"></i> Hotline: <a href="#">1900 068 898</a></p>
                  <p><i class="fas fa-envelope"></i> Email: <a href="#">cskh@hdinsurance.com.vn</a></p>
               </div>
            </div>
       </div>



       <Modal
         show={open1}
         onHide={() => setOpen1(false)}
         size="lg"
         aria-labelledby="contained-modal-title-vcenter"
         centered
       >
         <Modal.Header closeButton>
           <Modal.Title id="contained-modal-title-vcenter">
             Chính sách bảo mật
           </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <div className="content-pages-popup">
               <p>Chính sách bảo mật (‘Chính sách’) này đã được triển khai và cung cấp nhằm giúp bạn hiểu cách chúng tôi thu thập, sử dụng, quản lý và bảo vệ thông tin cá nhân của bạn, trong suốt quá trình tương tác với BẢO HIỂM HD qua trang web (<a href="https://hdinsurance.com.vn" target="_blank">www.hdinsurance.com.vn</a>), các ứng dụng mạng, hoặc các dịch vụ tương tác khác như ứng dụng di động (Mobile App). Xin lưu ý rằng, hầu hết các đơn đặt hàng tại BẢO HIỂM HD đều thông qua  yêu cầu mua hàng (Purchase order/PO) hoặc hợp đồng (Contract) với lượng khách hàng chủ yếu là doanh nghiệp (B2B). Do đó, phạm vi bảo mật đa phần được áp dụng theo hợp đồng đã ký kết.</p>
               <h6>Chính sách chung</h6>
               <p>BẢO HIỂM HD thu thập và sử dụng dữ liệu cá nhân của bạn trên website chỉ để xử lý các thoả thuận đặt mua hàng Online (nếu có và cổng mua online được mở) hoặc được ký kết với bạn qua báo phí, PO, hợp đồng và cho các mục đích tiếp thị. Nếu, vào bất kỳ thời điểm nào, bạn muốn xóa dữ liệu cá nhân hoặc sửa đổi, vui lòng gửi các chi tiết liên hệ cho chúng tôi vào email <a href="mailto:cskh@hdinsurance.com.vn" target="_blank">CSKH@hdinsurance.com.vn</a>.</p>
               <p>Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình. Ngoài ra, Khách hàng có trách nhiệm thông báo kịp thời cho BẢO HIỂM HD về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.</p>
               <h6>Thu thập và bảo vệ dữ liệu</h6>
               <p>BẢO HIỂM HD thu thập dữ liệu cần thiết cho các dịch vụ (thông qua đăng ký người dùng, bản tin) bao gồm tên, địa chỉ, điện thoại, email, ngày sinh và địa chỉ. Dữ liệu cũng được thu thập khi bạn truy cập trang web của BẢO HIỂM HD. BẢO HIỂM HD đã thực hiện các biện pháp bảo mật và kiểm soát chặt chẽ để bảo vệ dữ liệu cá nhân của bạn chống lại sự truy cập trái phép, lạm dụng, mất mát hay phá hủy.</p>
               <h6>Lưu trữ thông tin và di chuyển ngoài máy chủ được đặt tại Việt Nam. </h6>
               <p>BẢO HIỂM HD còn sử dụng các dịch vụ lưu trữ trực tuyến để cất giữ thông tin và mã hóa theo tiêu chuẩn của Microsoft OneDrive, Google Drive, Box, Dropbox, AWS. Tất cả dữ liệu được lưu trữ tại các nhà cung cấp uy tín và có độ tin cậy cao được chứng thực. BẢO HIỂM HD đảm bảo rằng tất cả các dữ liệu trên các máy chủ đó sẽ được lưu trữ phù hợp với các tiêu chuẩn bảo vệ do luật pháp quy định. Thời gian lưu trữ thông tin: lâu dài hoặc theo yêu cầu của khách hàng hủy bỏ.</p>
            </div>
         </Modal.Body>
         <Modal.Footer>
           <Button className="close-md-btn" onClick={()=>setOpen1(false)}>Đóng</Button>
         </Modal.Footer>
       </Modal>


       <Modal
         show={open2}
         onHide={() => setOpen2(false)}
         size="lg"
         aria-labelledby="contained-modal-title-vcenter"
         centered
       >
         <Modal.Header closeButton>
           <Modal.Title id="contained-modal-title-vcenter">
             Chính sách thanh toán
           </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <div className="content-pages-popup">
               <p>Quý Khách có thể thực hiện đóng phí bảo hiểm tại website: <a href="https://hdinsurance.com.vn" target="_blank">www.hdinsurance.com.vn</a>.</p>
               <p> Đây là trang Web của Bảo Hiểm HD để cung cấp kênh thanh toán phí bảo hiểm trực tuyến an toàn và hoàn toàn miễn phí.</p>
               <p>Kênh thanh toán này giúp Quý Khách thực hiện giao dịch nhanh chóng, chính xác, an toàn, mọi lúc, mọi nơi và loại bỏ các rủi ro về quản lý tiền mặt. Quý Khách có thể sử dụng các loại thẻ thanh toán (ATM), thẻ tín dụng (Credit Card), thẻ ghi nợ (Debit Card) của nhiều ngân hàng khác nhau để thanh toán phí bảo hiểm.</p>
               <p>Chúng tôi cung cấp 2 loại hình thanh toán bao gồm:</p>
               <ul>
                  <li>Cổng thanh toán trực tuyến: Thanh toán bằng các hình thức ví điện tử, các loại thẻ Ngân hàng (ATM, Credit Card,…)</li>
                  <li>Internet Banking: Thanh toán bằng cách chuyển khoản đến ngân hàng của chúng tôi.</li>
               </ul>
               <h6>Lưu ý khi sử dụng kênh thanh toán phí bảo hiểm trực tuyến:</h6>
               <ul className="list-ul-2">
                  <li>Quý Khách cần đảm bảo đáp ứng yêu cầu độ tuổi theo quy định của pháp luật để sử dụng và tự chịu trách nhiệm về các vấn đề phát sinh trong việc sử dụng dịch vụ thanh toán phí bảo hiểm trực tuyến.</li>
                  <li>Quý Khách cam kết là chủ sở hữu hợp pháp của thẻ tín dụng/thẻ ghi nợ/thẻ ATM/ví điện tử được sử dụng để thanh toán trực tuyến tại trang Web này.</li>
                  <li>Quý Khách cam kết cung cấp thông tin đầy đủ và chính xác khi thực hiện chức năng thanh toán trực tuyến. Nếu Quý Khách nhập sai thông tin, Chúng tôi có quyền từ chối giao dịch.</li>
                  <li>Quý Khách cam kết không sử dụng hệ thống thanh toán trực tuyến tại trang Web này để: 
                        <ul className="ul-list-3">
                           <li>Gây ảnh hưởng đến khả năng truy cập của người khác hoặc hoạt động của trang Web; hoặc</li>
                           <li>Cố tình sử dụng các biện pháp để giao dịch hoặc lấy thông tin không được phép; hoặc </li>
                           <li>Thực hiện bất kỳ hành động trái pháp luật nào khác.</li>
                        </ul>
                  </li>
                  <li>Bên cạnh các mục đích thu thập thông tin nêu trong Chính Sách Bảo Mật, Quý Khách đồng ý rằng chúng tôi có thể sử dụng thông tin cá nhân của Quý Khách cho mục đích xử lý và thực hiện các công việc liên quan đến việc thanh toán phí bảo hiểm trực tuyến. Thông tin cá nhân của Quý Khách sẽ được bảo vệ theo <a href="#" onClick={openMd1}>Chính Sách Bảo Mật</a>. Quý Khách có thể tham khảo nội dung <a href="#" onClick={openMd1}>Chính Sách Bảo Mật</a> tại đây.</li>
                  <li>Ngoại trừ trường hợp như quy định tại Mục 9 dưới đây, Quý Khách sẽ không phải chịu phí cho việc thanh toán phí bảo hiểm tại trang Web này. Bảo Hiểm HD sẽ không chịu trách nhiệm đối với các khoản phí khác có thể phát sinh do ngân hàng phát hành thẻ của Quý Khách thu thêm.</li>
                  <li>Nếu Quý Khách không phải là Bên Mua Bảo Hiểm của Hợp Đồng Bảo Hiểm mà Quý Khách thanh toán trực tuyến tại trang Web này, Quý Khách cam kết rằng Quý Khách đã được ủy quyền hợp lệ từ Bên Mua Bảo Hiểm để thanh toán cho chúng tôi. Để tránh hiểu nhầm, Quý Khách đồng ý và xác nhận rằng, tất cả các quyền lợi phát sinh liên quan đến Hợp Đồng Bảo Hiểm này đều thuộc về Người Được Bảo Hiểm hoặc những người thụ hưởng được quy định trong Hợp Đồng Bảo Hiểm. Theo đó, chúng tôi sẽ thanh toán cho Người Được Bảo Hiểm và/hoặc những người thụ hưởng có quyền nhận thanh toán theo quy định tại Hợp Đồng Bảo Hiểm mà chúng tôi đã ký với Bên Mua Bảo Hiểm. Quý Khách cam kết rằng Quý Khách sẽ không có bất kỳ khiếu nại hoặc khiếu kiện nào đối với Bảo Hiểm HD liên quan đến vấn đề này.</li>
                  <li>Liên quan đến các giao dịch gian lận hoặc giả mạo, chúng tôi sẽ thực hiện các biện pháp xử lý theo quy định của Tổ chức thanh toán thẻ và Tổ chức phát hành thẻ và theo đúng quy định của pháp luật.</li>
                  <li>Trong trường hợp Quý khách (Bên Mua Bảo Hiểm) yêu cầu hủy hợp đồng bảo hiểm mới trong thời gian 21 ngày kể từ ngày nhận được Hợp đồng, Quý khách sẽ được hoàn lại 70% số phí bảo hiểm đã đóng (không có lãi) trừ đi các chi phí có liên quan theo quy định. Nếu Quý khách thanh toán bằng thẻ thanh toán quốc tế, chúng tôi sẽ hoàn trả phí bảo hiểm đã đóng vào tài khoản thẻ thanh toán quốc tế của Quý khách sau khi đã trừ chi phí dịch vụ theo quy định của ngân hàng (nếu có).</li>
               </ul>
               <p>Quý Khách cam kết rằng việc Quý Khách thực hiện thanh toán trực tuyến trên trang Web của chúng tôi đồng nghĩa với việc Quý Khách đã đọc và đồng ý với Chính Sách Thanh Toán này.</p>
               <p>Nếu Quý Khách không đồng ý với bất kỳ nội dung nào của Chính Sách Thanh Toán này, Quý Khách vui lòng tạm dừng giao dịch và thoát khỏi trang thanh toán.</p>
            </div>
         </Modal.Body>
         <Modal.Footer>
           <Button className="close-md-btn" onClick={()=>setOpen2(false)}>Đóng</Button>
         </Modal.Footer>
       </Modal>






    </div>)
}
    
export default Footer;
