import PaymentInfo from './paymentinfo';
import TransferPopUpInfo from './transfer-popup';
import ChanelItem from './paymentchanel';
import Skeleton from 'react-loading-skeleton';
import Banner from './banner';
import { Button } from 'react-bootstrap';
import Ticket from './ticket';
import TransferInfo from './transfer';
import React, { useEffect, useState } from 'react';
import api from '../services/Network';
import LogsService from '../services/LogServices';
import cogoToast from 'cogo-toast';

const getWindowDimensions = () => {
  if (process.browser) {
    const { innerWidth: width, innerHeight: height } = window;
    return {
      width,
      height,
    };
  }
};

const useWindowDimensions = () => {
  if (process.browser) {
    const [windowDimensions, setWindowDimensions] = useState(
      getWindowDimensions()
    );

    useEffect(() => {
      function handleResize() {
        setWindowDimensions(getWindowDimensions());
      }

      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
    }, []);

    return windowDimensions;
  }
};

const request_id = LogsService.makeid();

const Main = (props) => {
  const [org, setOrg] = useState({});
  const [amountwarning, setWarning] = useState(null);
  let isMobile = false;

  if (process.browser) {
    const { width } = useWindowDimensions();
    isMobile = width < 992;
  }

  const onClickItem = (org_code, index) => {
    if (org.Org_Code != org_code) {
      const org_item = props.list_payment[index];
      setOrg(org_item);
      if (org_item.Min_Amount) {
        if (org_item.Min_Amount * 1 > props.payment_info.Total_Amount * 1) {
          return setWarning(org_item.Description_Min);
        } else {
          setWarning(null);
        }
      } else {
        if (amountwarning) {
          setWarning(null);
        }
      }
      if (org_item.Max_Amount) {
        if (org_item.Max_Amount * 1 < props.payment_info.Total_Amount * 1) {
          return setWarning(org_item.Description_Max);
        } else {
          setWarning(null);
        }
      } else {
        if (amountwarning) {
          setWarning(null);
        }
      }
    }
  };

  const gotoPayment = async () => {
    try {
      if (org.Org_Code) {
        const params = {
          ID: props.payment_info.Transaction_Id,
          ORG_CODE: org.Org_Code,
          LINK_CALLBACK: props.urlCb,
          lang: l.getLang() == 'vi' ? 'VN' : 'EN',
        };

        const data = await api.postLocal('/api/create-url-payment', params);

        LogsService.insert(
          request_id,
          `Go to Payment Request: ${params.ID}`,
          'info',
          'request',
          { url: '/payment/info', request: params, response: data }
        );

        if (data.success) {
          if (data.url_redirect) {
            cogoToast.loading('Đang chuyển hướng...', { hideAfter: 0 });
            window.location.href = data.url_redirect;
          } else {
            cogoToast.error('Payment is error');
          }
        } else {
          cogoToast.error('Payment is error');
        }
      } else {
        cogoToast.error(l.g('payform.error_no_channel'));
      }
    } catch (e) {
      LogsService.insert(
        request_id,
        `Go to Payment Error ${props.payment_info.Transaction_Id}`,
        'error',
        'request',
        { id: props.payment_info.Transaction_Id, error: e.toString() }
      );
    }
  };

  useEffect(() => {
    if (!isMobile) {
      setTimeout(() => {
        const rightCol = document.querySelector('#rightCol');
        const leftCol = document.querySelector('#leftCol');
        if (rightCol && leftCol) {
          const rightColHeight = Number.parseFloat(
            window.getComputedStyle(rightCol).height
          );
          leftCol.setAttribute('style', `min-height: ${rightColHeight}px`);
        }
      }, 100);
    }
  }, [isMobile]);

  return (
    <div>
      <div className='partice-bg' />
      <div className='pg-container'>
        <div className='row'>
          <div className='col-lg-8'>
            <div className='warn-note'>
              <i class='fas fa-exclamation-circle'></i>
              <p>{l.g('payform.warn_note')}</p>
            </div>
          </div>
        </div>
        <h5 className='h5-title'>
          {props.preload ? (
            <Skeleton width={228} height={24} />
          ) : (
            l.g('payform.payment_info')
          )}
        </h5>
        <div className='row'>
          <div className='col-lg-8'>
            <PaymentInfo
              data={props.payment_info}
              preload={props.preload}
              id='leftCol'
            />
            {props.payment_info.Bank_Account &&
              props.payment_info.Pay_Type !== 'CK' && (
                <TransferPopUpInfo data={props.payment_info} />
              )}
            {isMobile && props?.payment_info?.Pay_Type !== 'CK' && (
              <Ticket data={props.payment_info} preload={props.preload} />
            )}
            {isMobile && props?.payment_info?.Pay_Type === 'CK' && (
              <TransferInfo data={props.payment_info} preload={props.preload} />
            )}
            {props?.payment_info?.Pay_Type !== 'CK' && (
              <div>
                <h5 className='h5-title'>
                  {props.preload ? (
                    <Skeleton width={218} height={24} />
                  ) : (
                    l.g('payform.list_pay_channel')
                  )}
                </h5>
                <p className='select-payment-text'>
                  {props.preload ? (
                    <Skeleton width={168} height={22} />
                  ) : (
                    l.g('payform.pay_note_channel')
                  )}
                </p>
                <div
                  className={
                    props.list_payment.length > 0
                      ? 'payment-chanels'
                      : 'dl-none'
                  }
                >
                  <div className='row'>
                    {props.list_payment.map((item, index) => {
                      return (
                        <ChanelItem
                          key={index}
                          data={item}
                          checked={item.Org_Code == org.Org_Code}
                          index={index}
                          onClick={onClickItem}
                          preload={props.preload}
                        />
                      );
                    })}
                  </div>
                  {amountwarning ? (
                    <p className='min-required-label ta-center'>
                      {amountwarning}
                    </p>
                  ) : (
                    ''
                  )}
                  <div
                    style={{
                      textAlign: 'center',
                      padding: 30,
                      borderTop: '1px solid #EEEEEE',
                      marginTop: 20,
                      zIndex: 99,
                      position: 'relative',
                    }}
                  >
                    {props.preload ? (
                      <Skeleton
                        style={{ borderRadius: 24 }}
                        width={238}
                        height={44}
                      />
                    ) : (
                      <Button
                        className='go-to-payment'
                        onClick={(e) => gotoPayment()}
                      >
                        {l.g('payform.make_a_pay')}
                      </Button>
                    )}
                  </div>
                </div>
                <div
                  className={
                    props.list_payment.length == 0
                      ? 'payment-chanels empty-channel'
                      : 'dl-none'
                  }
                >
                  <h3>{l.g('payform.no_channel')}</h3>
                </div>
              </div>
            )}
          </div>
          <div className='col-lg-4'>
            {!isMobile && props?.payment_info?.Pay_Type !== 'CK' && (
              <Ticket data={props.payment_info} preload={props.preload} />
            )}
            {!isMobile && props?.payment_info?.Pay_Type === 'CK' && (
              <TransferInfo
                data={props.payment_info}
                preload={props.preload}
                id='rightCol'
              />
            )}
          </div>
        </div>
      </div>
      {/*---------------Banner container-------*/}
      <Banner />
      {/*---------------End banner container-------*/}
    </div>
  );
};

export default Main;
