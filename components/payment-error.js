import Layout from '../components/layout.js';
import { Button } from 'react-bootstrap';
import Head from 'next/head'
import ProductList from './product-list.js'
import React, { useEffect, useState } from "react";

 const PaymentError = (props) => {

    return (
        <div className="payment-status-container error">
          <div className="orange-banner"></div>
          <div className="container">
          <div className=" error-frame">
            <div className="img-err-obj">
              <img src="/img/error-object.png" />
            </div>
            <h1>{l.g("payform.payment_error")}</h1>
            <p>{l.g("payform.try_again")}</p>
            <a className={props.url_callback==null?"back-to-previous disabled":"back-to-previous"} href={props.url_callback} disabled={props.url_callback==null}>{l.g("payform.back_to_da_game")}</a>
            </div>
          </div>


          <ProductList/>

        </div>
    );}

export default PaymentError
