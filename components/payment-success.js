import Layout from '../components/layout.js';
import { Button } from 'react-bootstrap';
import Head from 'next/head'
import ProductList from './product-list.js'
import React, { useEffect, useState } from "react";

 const PaymentError = (props) => {
  const [counter, setCounter] = React.useState(15);
  React.useEffect(() => {
    if(props.url_callback){
        counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
      if(counter == 0){
          window.location.href = props.url_callback;
      }else{

      }
    }
  }, [counter]);

    return (
        <div className="payment-status-container success">
          <div className="green-banner"></div>
          <div className="container">
          <div className=" error-frame">
            <div className="img-err-obj">
              <img src="/img/sucess-object.png" />
            </div>
            <h1 className="text-green">{l.g("payform.payment_success")}</h1>
            {props.url_callback!=null?<p>{l.g("payform.success_note")} <span className="text-red">{counter} {l.g("payform.sec")}</span></p>:<p></p>}
            <div className="row">
              <div className="col-md-6 tr"><a className={props.url_callback==null?"back-to-previous disabled":"back-to-previous"} href={props.url_callback} disabled={props.url_callback==null}>{l.g("payform.back_to_da_game")}</a></div>
              <div className="col-md-6 tl"><a className="view-more-product" href="#">{l.g("payform.more_product")}</a></div>
            </div>

            </div>
          </div>


          <ProductList/>

        </div>
    );}

export default PaymentError
