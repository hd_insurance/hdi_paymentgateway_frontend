import { OverlayTrigger, Popover } from 'react-bootstrap';
import Skeleton from 'react-loading-skeleton';

const popover = (
  <Popover id="popover-basic">
    <Popover.Content>
     <span className="max-amount"> Số tiền tối đa được phép thanh toán qua Ví điện tử ViettelPay là 50.000.000 VNĐ</span>
    </Popover.Content>
  </Popover>
);

const PaymentInfo = (props)=>{
					return (<div className={props.checked ? "pc-item checked" : "pc-item"} onClick={()=>props.onClick(props.data.Org_Code, props.index)}>
					<div className="center-container mx-01">
						<div className="check-circle center-div"></div>
					</div>
					<div className="center-vertically  align-middle"><span>{props.data.Org_Name}</span></div>
					<div className="align-middle-icon">
						<img className="chanel-icon" src={props.data.Path_Logo} />
					</div>
				</div>)}

const PaymentInfoLoading = (props)=>{
					return (<div className="pc-item">
					<div className="center-container mx-01">
						<div className="center-div"><Skeleton circle={true} height={17} width={17} /></div>
					</div>
					<div className="center-vertically  align-middle"><Skeleton width="80%" height={10} style={{marginLeft:10}}/></div>
					<div className="align-middle-icon">
						<Skeleton height={36} width={36} style={{marginTop:8}}/>
					</div>
				</div>)}

export default (props) => (
	

		<div className="col-md-4 col-sm-6 col-xs-6 col-ssm-6">
				{props.preload?<PaymentInfoLoading/>:<PaymentInfo data={props.data} index={props.index} onClick={props.onClick} checked={props.checked}/>}
		</div>
	

	
	)