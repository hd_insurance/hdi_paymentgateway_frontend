import Skeleton from 'react-loading-skeleton';
import currencyFormatter from 'currency-formatter';

export default (props) => {
  const data = props.data || { Payer_Name: '' };

  return (
    <div className='payment-info' id={props?.id}>
      <div className='pi-header'>
        {props.preload ? (
          <Skeleton width={190} height={22} />
        ) : (
          l.g('payform.total_payment_amount')
        )}{' '}
        <span className='__amount'>
          {' '}
          {props.preload ? (
            <Skeleton width={90} height={22} />
          ) : (
            currencyFormatter.format(data.Total_Amount, {
              code: data.Currency,
              precision: data.Currency == 'USD' ? 2 : 0,
              format: '%v %s',
              symbol: data.Currency,
            })
          )}
        </span>
      </div>
      <div className='pi-body'>
        <div className='row'>
          <div className='col-md-6 col-sm-12'>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={100} height={20} />
                ) : (
                  l.g('payform.insur_amount')
                )}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <div>
                    <Skeleton width={110} height={22} />
                    <Skeleton width={130} height={22} />
                  </div>
                ) : (
                  currencyFormatter.format(data.Total_Amount, {
                    code: data.Currency,
                    precision: data.Currency == 'USD' ? 2 : 0,
                    format: '%v %s',
                    symbol: data.Currency,
                  }) +
                  ' (' +
                  data.Total_Amount_Text +
                  ')'
                )}{' '}
              </div>
            </div>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={90} height={20} />
                ) : (
                  l.g('payform.premium_reduction')
                )}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <Skeleton width={80} height={18} />
                ) : (
                  currencyFormatter.format(data.Discount, {
                    code: data.Currency,
                    precision: data.Currency == 'USD' ? 2 : 0,
                    format: '%v %s',
                    symbol: data.Currency,
                  })
                )}
              </div>
            </div>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? <Skeleton width={90} height={20} /> : 'VAT:'}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <Skeleton width={110} height={18} />
                ) : (
                  currencyFormatter.format(data.VAT, {
                    code: data.Currency,
                    precision: data.Currency == 'USD' ? 2 : 0,
                    format: '%v %s',
                    symbol: data.Currency,
                  })
                )}
              </div>
            </div>
          </div>
          <div className='col-md-6 col-sm-12'>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={90} height={20} />
                ) : (
                  l.g('payform.transaction_code')
                )}{' '}
              </div>
              <div className='l-right truncage-text'>
                {props.preload ? (
                  <Skeleton width={120} height={18} />
                ) : (
                  data.Order_Id
                )}
              </div>
            </div>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={100} height={20} />
                ) : (
                  l.g('payform.payer')
                )}{' '}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <Skeleton width={140} height={18} />
                ) : (
                  (data.Payer_Name ? data.Payer_Name : '').toUpperCase()
                )}
              </div>
            </div>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={100} height={20} />
                ) : (
                  l.g('payform.status')
                )}{' '}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <Skeleton width={110} height={18} />
                ) : (
                  data.Status_Name
                )}
              </div>
            </div>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={120} height={20} />
                ) : (
                  l.g('payform.request_time')
                )}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <div>
                    <Skeleton width={80} height={22} />
                    <Skeleton
                      style={{ marginLeft: 10 }}
                      width={50}
                      height={22}
                    />
                  </div>
                ) : (
                  data.Date_Pay
                )}
              </div>
            </div>
            <div className='l-row'>
              <div className='l-left'>
                {props.preload ? (
                  <Skeleton width={100} height={20} />
                ) : (
                  l.g('payform.pay_content')
                )}
              </div>
              <div className='l-right'>
                {props.preload ? (
                  <div>
                    <Skeleton width={60} height={22} />
                    <Skeleton
                      style={{ marginLeft: 10 }}
                      width={150}
                      height={22}
                    />
                  </div>
                ) : (
                  data.Description
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
