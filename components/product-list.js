const products = [1,2,3,4]


const ProductList = () => {
  return (
      <div className="products">
       <div className="partice-bg"/>
       <div className="container">
        <h3>Sản phẩm Nổi bật</h3>
         <ul className="row">
           {products.map(function(item, i){
            return <li key={i} className="col-md-3">
              <div className="product-item">
                <a href=""><img src="/img/insurance-product/i1.png" /></a>
                <div className="ifo">
                  <a href=""><h5>Bảo hiểm sức khỏe</h5></a>
                  <a className="view-detail" href=""> Xem chi tiết <i className="fas fa-arrow-right"></i></a>
                  <a className="view-insurance-benefits" href="">Xem quyền lợi bảo hiểm</a>
                </div>
              </div>

            </li>
          })}
         </ul>

         <div className="more-product">
            <a href="#">Xem thêm các sản phẩm khác</a>
         </div>
       </div>

      </div>
  );}

export default ProductList
