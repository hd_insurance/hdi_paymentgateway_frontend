import Skeleton from 'react-loading-skeleton';
import currencyFormatter from 'currency-formatter';

const Ticket = (props) => {
  const data = props.data;
  return (
    <article className='ticket'>
      <header className='ticket__wrapper'>
        <div className='ticket__header'>
          <div className='ticket-img'>
            {props.preload ? (
              <Skeleton width={208} height={208} />
            ) : (
              <img
                className='qr-img'
                src={`data:image/jpg;base64,${data.Qr_Code}`}
              />
            )}
            <p style={{ marginBottom: 0 }}>
              {props.preload ? (
                <Skeleton width={228} height={20} />
              ) : (
                l.g('payform.scan_to_pay_fast')
              )}
            </p>
            <p className='expire-qr'>
              {props.preload ? (
                <Skeleton width={188} height={20} />
              ) : (
                `${l.g('payform.expire')} ${data.Qr_Expire}`
              )}
            </p>
          </div>
        </div>
      </header>
      <div className='ticket__divider'>
        <div className='ticket__notch'></div>
        <div className='ticket__notch ticket__notch--right'></div>
      </div>
      <footer className='ticket__footer'>
        <h3>
          {props.preload ? (
            <Skeleton width={188} height={25} />
          ) : (
            l.g('payform.qr_code_info')
          )}
        </h3>
        <div className='qr-info'>
          <div className='qr1'>
            {props.preload ? (
              <Skeleton width={88} height={20} />
            ) : (
              l.g('payform.recipient')
            )}
          </div>
          <div className='qr2'>
            {props.preload ? (
              <Skeleton width={188} height={20} />
            ) : (
              l.g('payform.page_title_mobile_2')
            )}
          </div>
        </div>
        <div className='qr-info'>
          <div className='qr1'>
            {props.preload ? (
              <Skeleton width={98} height={20} />
            ) : (
              l.g('payform.amount')
            )}{' '}
          </div>
          <div className='qr2'>
            {props.preload ? (
              <Skeleton width={128} height={20} />
            ) : (
              currencyFormatter.format(data.Total_Amount, {
                code: data.Currency,
                precision: data.Currency == 'USD' ? 2 : 0,
                format: '%v %s',
                symbol: data.Currency,
              })
            )}
          </div>
        </div>
        {data.Min_Amount * 1 > data.Total_Amount * 1 ? (
          <p className='min-required-label'>{data.Description_Min}</p>
        ) : (
          ''
        )}
      </footer>
    </article>
  );
};

export default Ticket;
