import React, { useState } from 'react';
import currencyFormatter from 'currency-formatter';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import {
  Modal,
  Container,
  Button,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';

const TransferInfo = (props) => {
  const [open, setOpen] = useState(false);
  const [isCopyAll, setIsCopyAll] = useState(false);
  const data = props?.data;

  const text_copy_all =
    l.g('transfer_info.trans_note') +
    ' \n' +
    l.g('transfer_info.bank_acc') +
    ' ' +
    data.Bank_Account +
    ' \n' +
    l.g('transfer_info.bank_name') +
    ' ' +
    data.Bank_Name +
    ' ' +
    data.Bank_Branch +
    ' \n' +
    l.g('transfer_info.account') +
    ' ' +
    data.Recipient +
    ' \n' +
    l.g('transfer_info.content') +
    ' ' +
    data.Content_Pay +
    ' \n' +
    l.g('transfer_info.amount') +
    ' ' +
    currencyFormatter.format(data.Total_Amount, {
      code: data.Currency,
      precision: data.Currency == 'USD' ? 2 : 0,
      format: '%v %s',
      symbol: data.Currency,
    });

  const showPopPaymentTransfer = () => {
    setOpen(true);
  };

  const CoppyButton = (props) => {
    const [copied, setCopied] = useState(false);
    const text = props.text;

    return (
      <div className='aabcc'>
        <OverlayTrigger
          key={'top'}
          placement={'top'}
          onToggle={(isShow) => {
            if (isShow) {
              setCopied(false);
            }
          }}
          overlay={
            <Tooltip id={`tooltip-top`}>
              {!copied
                ? l.g('transfer_info.copy')
                : l.g('transfer_info.copied')}
            </Tooltip>
          }
        >
          <CopyToClipboard
            text={text}
            onCopy={() => {
              setCopied(true);
            }}
          >
            <div className='copy-btn'>
              <i class='fas fa-copy'></i>
            </div>
          </CopyToClipboard>
        </OverlayTrigger>
      </div>
    );
  };

  return (
    <div className='transfer-pop'>
      {data?.Pay_Type !== 'CK' && (
        <p className='transfer-guide' onClick={(e) => showPopPaymentTransfer()}>
          {l.g('transfer_info.trans_guide')}
        </p>
      )}
      <Modal
        show={open}
        onHide={() => setOpen(false)}
        aria-labelledby='contained-modal-title-vcenter'
      >
        <Modal.Header closeButton>
          <Modal.Title id='contained-modal-title-vcenter'>
            {l.g('transfer_info.md_title')}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='show-grid'>
          <Container>
            <p className='transnote'>{l.g('transfer_info.trans_note')}</p>
            <div className='row'>
              <div className='col-md-4 info-l'>
                {l.g('transfer_info.bank_acc')}
              </div>
              <div className='col-md-8 info-r'>
                <div className='ifcontent'> {data.Bank_Account}</div>

                <CoppyButton text={data.Bank_Account} />
              </div>
            </div>
            <div className='row mt-10'>
              <div className='col-md-4 info-l'>
                {l.g('transfer_info.bank_name')}
              </div>
              <div className='col-md-8 info-r'>
                <div className='ifcontent'>{`${data.Bank_Name} - ${data.Bank_Branch}`}</div>

                <CoppyButton text={`${data.Bank_Name} - ${data.Bank_Branch}`} />
              </div>
            </div>
            <div className='row mt-10'>
              <div className='col-md-4 info-l'>
                {l.g('transfer_info.account')}
              </div>
              <div className='col-md-8 info-r'>
                <div className='ifcontent'>{data.Recipient}</div>

                <CoppyButton text={data.Recipient} />
              </div>
            </div>
            <div className='row mt-10'>
              <div className='col-md-4 info-l'>
                {l.g('transfer_info.content')}
              </div>
              <div className='col-md-8 info-r'>
                <div className='ifcontent'>{data.Content_Pay}</div>

                <CoppyButton text={data.Content_Pay} />
              </div>
            </div>
            <div className='row mt-10'>
              <div className='col-md-4 info-l'>
                {l.g('transfer_info.amount')}
              </div>
              <div className='col-md-8 info-r'>
                <span className='text-red'>
                  {currencyFormatter.format(data.Total_Amount, {
                    code: data.Currency,
                    precision: data.Currency == 'USD' ? 2 : 0,
                    format: '%v %s',
                    symbol: data.Currency,
                  })}
                </span>

                <CoppyButton text={data.Total_Amount} />
              </div>
            </div>
          </Container>
        </Modal.Body>
        <div className='md-footer'>
          <div className='row'>
            <div className='col-md-6'>
              <CopyToClipboard
                text={text_copy_all}
                onCopy={() => {
                  setIsCopyAll(true);
                  setTimeout(() => setIsCopyAll(false), 1000);
                }}
              >
                <Button className='btn-1'>
                  {isCopyAll ? (
                    <>
                      <i class='far fa-check-circle'></i>{' '}
                      {l.g('transfer_info.copied')}
                    </>
                  ) : (
                    <>
                      <i class='fas fa-copy'></i>{' '}
                      {l.g('transfer_info.copy_all')}
                    </>
                  )}
                </Button>
              </CopyToClipboard>
            </div>
            <div className='col-md-6 mtt-15'>
              <Button onClick={() => setOpen(false)} className='btn-2'>
                {l.g('transfer_info.close')}
              </Button>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default TransferInfo;
