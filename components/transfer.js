import React, { useState } from 'react';
import currencyFormatter from 'currency-formatter';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Container, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';

const TransferInfo = (props) => {
  const [isCopyAll, setIsCopyAll] = useState(false);
  const data = props?.data;

  const text_copy_all =
    l.g('transfer_info.trans_note') +
    ' \n' +
    l.g('transfer_info.bank_acc') +
    ' ' +
    data.Bank_Account +
    ' \n' +
    l.g('transfer_info.bank_name') +
    ' ' +
    data.Bank_Name +
    ' ' +
    data.Bank_Branch +
    ' \n' +
    l.g('transfer_info.account') +
    ' ' +
    data.Recipient +
    ' \n' +
    l.g('transfer_info.content') +
    ' ' +
    data.Content_Pay +
    ' \n' +
    l.g('transfer_info.amount') +
    ' ' +
    currencyFormatter.format(data.Total_Amount, {
      code: data.Currency,
      precision: data.Currency == 'USD' ? 2 : 0,
      format: '%v %s',
      symbol: data.Currency,
    });

  const CoppyButton = (props) => {
    const [copied, setCopied] = useState(false);
    const text = props.text;

    return (
      <div className='aabcc d-inline-block float-right'>
        <OverlayTrigger
          key={'top'}
          placement={'top'}
          onToggle={(isShow) => {
            if (isShow) {
              setCopied(false);
            }
          }}
          overlay={
            <Tooltip id={`tooltip-top`}>
              {!copied
                ? l.g('transfer_info.copy')
                : l.g('transfer_info.copied')}
            </Tooltip>
          }
        >
          <CopyToClipboard
            text={text}
            onCopy={() => {
              setCopied(true);
            }}
          >
            <div className='copy-btn text-success'>
              <i class='fas fa-copy' />
            </div>
          </CopyToClipboard>
        </OverlayTrigger>
      </div>
    );
  };

  return (
    <div className='payment-info' id={props?.id}>
      <Container>
        <div className='w-100 mb-4'>
          <strong>{l.g('transfer_info.transfer_pay_order')}</strong>
        </div>
        <p className='transnote'>{l.g('transfer_info.trans_note')}</p>
        <div className='row transfer_row'>
          <div className='col-md-4 info-l'>{l.g('transfer_info.bank_acc')}</div>
          <div className='col-md-8 info-r'>
            <div className='ifcontent'> {data.Bank_Account}</div>
            <CoppyButton text={data.Bank_Account} />
          </div>
        </div>
        <div className='row transfer_row'>
          <div className='col-md-4 info-l'>
            {l.g('transfer_info.bank_name')}
          </div>
          <div className='col-md-8 info-r'>
            <div className='ifcontent'>{`${data.Bank_Name} - ${data.Bank_Branch}`}</div>
            {/* <CoppyButton text={`${data.Bank_Name} - ${data.Bank_Branch}`} /> */}
          </div>
        </div>
        <div className='row transfer_row'>
          <div className='col-md-4 info-l'>{l.g('transfer_info.account')}</div>
          <div className='col-md-8 info-r'>
            <div className='ifcontent'>{data.Recipient}</div>
            <CoppyButton text={data.Recipient} />
          </div>
        </div>
        <div className='row transfer_row'>
          <div className='col-md-4 info-l'>{l.g('transfer_info.content')}</div>
          <div className='col-md-8 info-r'>
            <div className='ifcontent'>{data.Content_Pay}</div>
            <CoppyButton text={data.Content_Pay} />
          </div>
        </div>
        <div className='row transfer_row'>
          <div className='col-md-4 info-l'>{l.g('transfer_info.amount')}</div>
          <div className='col-md-8 info-r'>
            <span className='transfer_amount'>
              {currencyFormatter.format(data.Total_Amount, {
                code: data.Currency,
                precision: data.Currency == 'USD' ? 2 : 0,
                format: '%v %s',
                symbol: data.Currency,
              })}
            </span>
            <CoppyButton text={data.Total_Amount} />
          </div>
        </div>
      </Container>
      <div className='md-footer text-center'>
        <CopyToClipboard
          text={text_copy_all}
          onCopy={() => {
            setIsCopyAll(true);
            setTimeout(() => setIsCopyAll(false), 1000);
          }}
        >
          <Button className='btn-1 w-auto'>
            {isCopyAll ? (
              <>
                <i class='far fa-check-circle'></i>{' '}
                {l.g('transfer_info.copied')}
              </>
            ) : (
              <>
                <i class='fas fa-copy'></i> {l.g('transfer_info.copy_all')}
              </>
            )}
          </Button>
        </CopyToClipboard>
      </div>
    </div>
  );
};

export default TransferInfo;
