import 'bootstrap/dist/css/bootstrap.min.css';
import '../components/app.css'
import '../components/reponsive.css'
import Language from "../services/Language.js";
import { useRouter } from "next/router";


function MyApp({ Component, pageProps }) {
  global.l = new Language();
  const router = useRouter();
  const {lng} = router.query;
  if(lng){
    if(lng.toLowerCase() == "vi"){
      global.l.setLang("vi");
    }else{
      global.l.setLang("en");
    }
  }
  return <Component {...pageProps} />
}
export default MyApp