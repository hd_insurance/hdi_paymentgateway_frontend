import router from "next/router";
import Layout from "../components/layout.js";
import Main from "../components/main.js";
import PaymentError from "../components/payment-error.js";
import PaymentSuccess from "../components/payment-success.js";
import { Button } from "react-bootstrap";
import Head from "next/head";


import Ticket from "../components/ticket.js";
import React, { useEffect, useState } from "react";
import api from "../services/Network.js";
import LogsService from "../services/LogServices.js";
import socketIOClient from "socket.io-client";
import config from "../config";
const ENDPOINT = "";



// const ENDPOINT = "http://localhost:3000";
var count = 0;

const request_id = LogsService.makeid();

const Index = () => {
  const [getLstPayGateway, setLstPayGateway] = useState([1, 2, 3, 4, 5, 6]);
  const [PayInfo, setPayInfo] = useState({});
  const [preload, setPreload] = useState(true);
  const [urlCb, setUrlCb] = useState(null);
  const [pageState, setPageState] = useState(0);
  const [thrPayinfo, setThrPayinfo] = useState(null);
  

  useEffect(() => {
    try {
      if (router.router == null) {
        return setPageState(1)
      } else {

        if(router.router.query.vnp_SecureHash){
          setThrPayinfo(router.router.asPath)
          check3RdPayment(router.router.asPath)
        }else if (!router.router.query.id) {
          LogsService.insert(
            request_id?request_id:LogsService.makeid(),
            `GET Payment Error: Null Id`,
            "error",
            "request",
            "Null ID payment"
          )

          return setPageState(1)
        }
        if(router.router.query.callback){
          setUrlCb(router.router.query.callback)
        }

        if (router.router.query.id) {
           if (preload) {
              getInitData(router.router.query.id);
              setupSocketIo(router.router.query.id)
            }
        }


       
      }
    } catch (e) {
      LogsService.insert(
        request_id,
        `GET Payment Error ${router.router.query.id}`,
        "error",
        "request",
        {id: router.router.query.id, error: e.toString()}
      )
      setPageState(1)
    }
  });


  const getInitData = async (orderid) => {
    try {
      const data = await api.post("/payment/info", { lang: l.getLang()=="vi"?"VN":"EN", id: orderid });
      count++;
      LogsService.insert(
        request_id,
        `GET Payment Info: ${orderid}`,
        "info",
        "request",
        {url:"/payment/info", request: { lang: l.getLang()=="vi"?"VN":"EN", id: orderid }, response: data}
      )
      setPreload(false);
      if(data.Data.lstPayGateway){
      	setLstPayGateway(data.Data.lstPayGateway);
      }
      if(data.Data.lstPayInfo[0]){
      	setPayInfo(data.Data.lstPayInfo[0]);
      }

      

      
    } catch (e) {
      console.log("Payment Err ", orderid, e)
      LogsService.insert(
        request_id,
        `GET Payment Error: ${orderid}`,
        "error",
        "request",
        {error: e.toString()}
      )
      setPageState(1)
    }
  };

  const check3RdPayment = async (str_query) => {
    try {
      const data = await api.get("/payment/checkStatusPay"+str_query);
      if(data.Success){
        if(data.Data.LINK_CALLBACK){
          setUrlCb(data.Data.LINK_CALLBACK)
        }
        setPageState(2)
      }else{
        LogsService.insert(
          request_id,
          `3RD Callback Payment Error`,
          "error",
          "request",
          {str_query: str_query, data: data }
        )
        setPageState(1)
      }
      
    } catch (e) {
       LogsService.insert(
          request_id,
          `3RD Callback Payment Error (2)`,
          "error",
          "request",
          {str_query: str_query?str_query:"Empty string query from 3rd services", error: e.toString()}
        )
      setPageState(1)
    }
  };





  const setupSocketIo = async (orderid) => {
  	const socket = socketIOClient(`${ENDPOINT}?pm_id=${orderid}`);
    socket.on('connect', socketclient => {
		 socket.on('payment_status', (msg) => {
      LogsService.insert(
        request_id,
        `SOCKET Payment: ${orderid}`,
        "info",
        "request",
        msg
      )
      if(router.router.query.callback){
          setUrlCb(router.router.query.callback)
      }else{
        if(msg.url_callback){
          setUrlCb(msg.url_callback)
        }else{
          setUrlCb(null)
        }
      }
		    if(msg.status == 1){
		   	setPageState(2)
		   }else if(msg.status == 0){
		   	setPageState(1)
		   }
		  });
	});
  }



  function Content(props) {
	  switch(props.state){
		  	case 1:
		  		return <PaymentError url_callback={urlCb}/>
		  	case 2:
		  		return <PaymentSuccess url_callback={urlCb}/>
		  	default: 
		  		return <Main
		          list_payment={getLstPayGateway}
		          payment_info={PayInfo}
		          preload={preload}
              urlCb={urlCb}
		        />;
		}
	}




  return (
    <Layout>
      <Head>
        <title>HDI - Payment Gate</title>
        <meta name="viewport" content="user-scalable=no, width=device-width" />
        <link rel="stylesheet" href="/css/app.css"/>
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-NZET4YBR84"></script>
        <script
            async
            dangerouslySetInnerHTML={{
              __html: `window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-NZET4YBR84');`
            }}
          />



      </Head>
      <Content state={pageState}/>
    </Layout>
  );
};
Index.getInitialProps = async (ctx) => {
  
  return { stars: null }
}

export default Index;
