const express = require('express')
var bodyParser = require('body-parser')
const routes = require("./backend/router");
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const nextApp = next({ dev })
const nextHandler = nextApp.getRequestHandler()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(routes);

// socket.io server
io.on('connection', socket => {
  socket.join(socket.handshake.query.pm_id);
})

app.post('/callback', (req, res) => {
   if(req.body.id){
     let object = {
        id: req.body.id,
        status: req.body.status,
        url_callback: req.body.url_callback
     }
    let sent = io.to(req.body.id).emit('payment_status', object);
   }
   
   res.send({send:true})
})

nextApp.prepare().then(() => {
  app.use(express.static('public'))


  app.get('*', (req, res) => {
    return nextHandler(req, res)
  })

  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})